package usecase

import (
	"context"
	"gitlab.com/otus_golang/scanner/inernal/scanner/event"
	"time"

	"gitlab.com/otus_golang/scanner/app"
)

type EventUsecase struct {
	App            *app.App
	eventRepo      event.Repository
	queue          event.Queue
	contextTimeout time.Duration
}

func NewEventUsecase(app *app.App, eventRepo event.Repository, queue event.Queue, contextTimeout time.Duration) *EventUsecase {
	return &EventUsecase{App: app, eventRepo: eventRepo, queue: queue, contextTimeout: contextTimeout}
}

func (e *EventUsecase) EmitEvent(ctx context.Context, date time.Time) error {
	ctx, cancel := context.WithTimeout(ctx, e.contextTimeout)
	defer cancel()

	events, err := e.eventRepo.FindByDate(ctx, date)
	if err != nil {
		return err
	}

	for _, ev := range events {
		err := e.queue.EmitEvent(&ev)

		if err != nil {
			return err
		}
	}
	return nil
}
