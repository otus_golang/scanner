package event

import (
	"context"
	"time"
)

type Usecase interface {
	EmitEvent(ctx context.Context, date time.Time) error
}
