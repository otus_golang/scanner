package repository

import (
	"context"
	"gitlab.com/otus_golang/scanner/app"
	"gitlab.com/otus_golang/scanner/models"
	"log"
	"time"
)

type PsqlEventRepository struct {
	app *app.App
}

func NewPsqlEventRepository(app *app.App) *PsqlEventRepository {
	return &PsqlEventRepository{app: app}
}

func (p PsqlEventRepository) FindByDate(ctx context.Context, date time.Time) ([]models.Event, error) {
	query := `
		SELECT id, name, description, date 
		FROM events 
		WHERE date(date) = to_timestamp($1, 'YYYY-MM-DD')
	`

	rows, err := p.app.Db.QueryxContext(ctx, query, date)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	events := make([]models.Event, 0, 1)

	for rows.Next() {
		var event models.Event
		err := rows.StructScan(&event)
		if err != nil {
			log.Fatal(err)
		}
		events = append(events, event)
	}

	return events, nil
}
