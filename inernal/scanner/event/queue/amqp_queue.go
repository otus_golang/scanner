package queue

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/otus_golang/scanner/app"
	"gitlab.com/otus_golang/scanner/models"
)

type AmqpEventQueue struct {
	app *app.App
}

func NewAmqpEventQueue(app *app.App) *AmqpEventQueue {
	return &AmqpEventQueue{app: app}
}

func (a AmqpEventQueue) EmitEvent(event *models.Event) error {
	ch, err := a.app.Amqp.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		a.app.Config.Queue["exchange"], // name
		"fanout",                       // type
		true,                           // durable
		false,                          // auto-deleted
		false,                          // internal
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return err
	}

	body, err := json.Marshal(event)
	if err != nil {
		return err
	}

	err = ch.Publish(
		a.app.Config.Queue["exchange"], // exchange
		"",                             // routing key
		false,                          // mandatory
		false,                          // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})
	if err != nil {
		return err
	}

	a.app.Logger.Info(fmt.Sprintf(" [x] Sent %s", body))

	return nil
}
