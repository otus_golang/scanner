package event

import (
	"gitlab.com/otus_golang/scanner/models"
)

type Queue interface {
	EmitEvent(event *models.Event) error
}
