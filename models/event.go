package models

type Event struct {
	Id          uint32
	Name        string
	Description string
	Date        string
}
