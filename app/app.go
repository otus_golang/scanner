package app

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/streadway/amqp"

	"github.com/spf13/viper"
	"gitlab.com/otus_golang/scanner/config"
	"go.uber.org/zap"
)

type App struct {
	Config *config.Config
	Logger *zap.Logger
	Db     *sqlx.DB
	Amqp   *amqp.Connection
}

func (a *App) Run() {

	fmt.Print(viper.AllSettings())
	fmt.Print(a.Config)
	fmt.Print("Hello, world!\n")
}
