package cmd

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
	"github.com/streadway/amqp"
	"gitlab.com/otus_golang/scanner/config"
	db2 "gitlab.com/otus_golang/scanner/db"
	"gitlab.com/otus_golang/scanner/logger"
	queue2 "gitlab.com/otus_golang/scanner/queue"
	"go.uber.org/zap"
	"log"
)

var c *config.Config
var l *zap.Logger
var db *sqlx.DB
var amqpQueue *amqp.Connection

func init() {
	initConfig()
	initLogger(c)
	initDb(c)
	initQueue(c)
}

func initConfig() {
	var err error

	c, err = config.GetConfig()

	if err != nil {
		log.Fatalf("unable to load config: %v", err)
	}
}

func initLogger(c *config.Config) {
	var err error
	l, err = logger.GetLogger(c)

	if err != nil {
		log.Fatalf("unable to load logger: %v", err)
	}
}

func initDb(c *config.Config) {
	var err error
	db, err = db2.GetDb(c)

	if err != nil {
		log.Fatalf("unable to load db: %v", err)
	}
}

func initQueue(c *config.Config) {
	amqpQueue = queue2.GetConnection(c)
}

var rootCmd = &cobra.Command{
	Use:   "scanner",
	Short: "scanner service",
	Long:  `scanner service`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print("Use scanner scan\nRun 'scanner --help' for usage.\n")
	},
}

func Execute() {
	defer amqpQueue.Close()
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("root execute error: %v", err)
	}
}
