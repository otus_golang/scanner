package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"gitlab.com/otus_golang/scanner/app"
	"gitlab.com/otus_golang/scanner/inernal/scanner/event/queue"
	"gitlab.com/otus_golang/scanner/inernal/scanner/event/repository"
	"gitlab.com/otus_golang/scanner/inernal/scanner/event/usecase"
	"time"
)

var a *app.App

func init() {
	rootCmd.AddCommand(serve)
	initApp()
}

func initApp() {
	appInstance := app.App{
		Config: c,
		Logger: l,
		Db:     db,
		Amqp:   amqpQueue,
	}

	a = &appInstance
}

var serve = &cobra.Command{
	Use:   "scan",
	Short: "Start scanner server",
	Long:  `Start grpc scanner server`,
	Run: func(cmd *cobra.Command, args []string) {
		var contextTimeout time.Duration
		if a.Config.IsDevelopment() {
			contextTimeout = time.Hour
		} else {
			contextTimeout = time.Millisecond * 500
		}

		r := repository.NewPsqlEventRepository(a)
		q := queue.NewAmqpEventQueue(a)
		u := usecase.NewEventUsecase(a, r, q, contextTimeout)
		ctx := context.Background()

		t := time.Now()
		n := time.Date(t.Year(), t.Month(), t.Day(), 12, 0, 0, 0, t.Location())
		d := n.Sub(t)
		if d < 0 {
			n = n.Add(24 * time.Hour)
			d = n.Sub(t)
		}
		for {
			err := u.EmitEvent(ctx, time.Now())
			if err != nil {
				a.Logger.Error(err.Error())
			}

			time.Sleep(d)
		}

	},
}
