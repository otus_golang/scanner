module gitlab.com/otus_golang/scanner

go 1.13

require (
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	go.uber.org/zap v1.10.0
	google.golang.org/grpc v1.21.0
)
